from django import forms
from django.core.validators import RegexValidator
from .models import *

class SignUpForm(forms.ModelForm):

    class Meta:
        model = ChatBookUsers
        fields = ('username', 'email', 'first_name', 'last_name', 'mobile', 'password')