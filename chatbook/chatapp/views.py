from django.shortcuts import render, render_to_response, redirect
import json
from .models import *
from django.contrib.auth import authenticate, login, logout
import os
from .forms import *
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.core.mail import  send_mail
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.views import View
import jwt
from django.contrib.sessions.models import Session
import uuid
from copy import deepcopy
import sys
from twilio.rest import Client
import datetime
import MySQLdb
import string
import random
from subprocess import check_output
from copy import deepcopy
from django.views.decorators.csrf import csrf_exempt
from itertools import chain
from rest_framework.views import APIView

class PullImage(APIView):
    
    def get(self,request):
		dir_path = os.path.abspath(os.path.dirname(sys.argv[0]))
		image_path = dir_path+"/image"
		files = os.listdir(image_path)
		file_name = "firmware_image.img"
		for name in files:
			if name == file_name:
				download_path = os.path.join(image_path, file_name)
				FilePointer = open(download_path, "r")
				response= HttpResponse(FilePointer,content_type='application/octet-stream')
				response['Content-Disposition'] = 'attachment; filename=%s' % file_name
				return response

@csrf_exempt
@login_required
def reset_notification_counter(request):
	if request.method == "POST":
		params = request.POST
		admin = params["admin"]
		reset_user = params["reset_user"]
		filename = admin+".json"
		dir_path = os.path.abspath(os.path.dirname(sys.argv[0]))
		notification_path = os.path.join(dir_path, "notifications")
		for each in os.listdir(notification_path):
			if each == filename:
				path = os.path.join(notification_path, filename)
				iter_file = open(path, 'r+')
				iter_data = json.load(iter_file)
				for client in iter_data["rest_clients"]:
					if client["username"] == reset_user:
						client["notification"] = 0
						iter_file.seek(0)
						json.dump(iter_data, iter_file, indent = 4, sort_keys = True)
						iter_file.truncate()
						break
				notification_list = iter_data["rest_clients"]
				iter_file.close()
				return JsonResponse({'status':'200', 'notification_list':notification_list})

@csrf_exempt
@login_required
def set_notification_counter(request):
	if request.method == "POST":
		params = request.POST
		exception = params["exception"]
		user_inprocess = params["user_inprocess"]
		admin = params["admin"]
		filename = admin+".json"
		dir_path = os.path.abspath(os.path.dirname(sys.argv[0]))
		notification_path = os.path.join(dir_path, "notifications")
		for each in os.listdir(notification_path):
			if each == filename:
				path = os.path.join(notification_path, filename)
				iter_file = open(path, 'r+')
				iter_data = json.load(iter_file)
				if exception == "none":
					for client in iter_data["rest_clients"]:
						if client["username"] == user_inprocess:
							client["notification"] += 1
							iter_file.seek(0)
							json.dump(iter_data, iter_file, indent = 4, sort_keys = True)
							iter_file.truncate()
							break
					notification_list = iter_data["rest_clients"]
					iter_file.close()
					return JsonResponse({'status':'200', 'notification_list':notification_list})
				else:
					for client in iter_data["rest_clients"]:
						if client["username"] == user_inprocess:
							if exception == user_inprocess:
								client["notification"] = 0
								iter_file.seek(0)
								json.dump(iter_data, iter_file, indent = 4, sort_keys = True)
								iter_file.truncate()
								break
							else:
								client["notification"] += 1
								iter_file.seek(0)
								json.dump(iter_data, iter_file, indent = 4, sort_keys = True)
								iter_file.truncate()
								break
					notification_list = iter_data["rest_clients"]
					iter_file.close()
					return JsonResponse({'status':'200', 'notification_list':notification_list})


@csrf_exempt
@login_required
def notification_pull(request):
	if request.method == "POST":
		params = request.POST
		username = params["username"]
		dir_path = os.path.abspath(os.path.dirname(sys.argv[0]))
		notification_path = os.path.join(dir_path, "notifications")
		file_name = username+".json"
		for each in os.listdir(notification_path):
			if each == file_name:
				path = os.path.join(notification_path, file_name)
				iter_file = open(path, 'r+')
				iter_data = json.load(iter_file)
				notification_list = iter_data["rest_clients"]
				iter_file.close()
				return JsonResponse({'status':'200', 'notification_list':notification_list})

@csrf_exempt
@login_required(login_url='/login')
def is_token_valid(request):
	if request.method == "POST":
		user = request.user
		params = request.POST
		try:
			rec_token = params["token"]
		except:
			return JsonResponse({'status':'400', 'message':'Error Occured'})
		if rec_token == user.token:
			return JsonResponse({'status':'200', 'valid':1})
		else:
			return JsonResponse({'status':'401', 'valid':0})

def sidmaker(size = 16, chars = string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def tokengen(size = 50, chars = string.digits):
	return ''.join(random.choice(chars) for _ in range(size))

def signup_user(request):
	if request.method == "GET":
		return render(request, "chatbook_signup.html")
	if request.method == "POST":
		params = request.POST
		form = SignUpForm(params)
		if form.is_valid():
			form_data = form.cleaned_data
			temp = form.save(commit = False)
			temp.set_password(form_data["password"])
			temp.uuid = str(uuid.uuid4())
			try:
				connect_auth_db = MySQLdb.connect(host="127.0.0.1", user="auth_plugin", passwd="authauth", db="MQTTACL")
			except:
				return HttpResponse("Problem connecting to PHP database")
			if connect_auth_db:
				print("Connected to database !")
				cursor = connect_auth_db.cursor()
				sid = sidmaker()
				temp.sid= sid
				sql_pas = check_output(["np", "-p", sid])
				sql_pass = sql_pas.strip()
				username = temp.username
				super_status = 0
				topic = 'chatbook/#'
				rw_status = 4
				cursor.execute("INSERT INTO `mqtt_users`(`username`, `pw`, `super`) VALUES (%s, %s, %s)", (username, sql_pass, super_status))
				cursor.execute("INSERT INTO `user_acls`(`username`, `topic`, `rw`) VALUES (%s, %s, %s)", (username, topic, rw_status))
				queryset = ChatBookUsers.objects.all().exclude(username = temp.username)
				not_dict = {}
				not_list = []
				for rest_client in queryset:
					not_dict["username"] = rest_client.username
					not_dict["notification"] = 0
					not_list.append(not_dict.copy())
				data = {"account_holder":temp.username, "rest_clients":not_list}
				dir_path = os.path.abspath(os.path.dirname(sys.argv[0]))
				notification_path = os.path.join(dir_path, "notifications")
				file_name = temp.username+".json"
				path = os.path.join(notification_path, file_name)
				with open(path, 'w+') as user_file:
					json.dump(data, user_file, indent = 4, sort_keys = True)
				user_file.close()
				for filename in os.listdir(notification_path):
					if filename.endswith(".json"):
						if not filename.split(".")[0] == temp.username:
							temp_dict = {}
							temp_dict["username"] = temp.username
							temp_dict["notification"] = 0 
							iter_path = os.path.join(notification_path, filename)
							iter_file = open(iter_path, 'r+')
							iter_data = json.load(iter_file)
							iter_data["rest_clients"].append(temp_dict.copy())
							iter_file.seek(0)
							json.dump(iter_data, iter_file, indent = 4, sort_keys = True)
							iter_file.truncate()
							iter_file.close()
				#Final Commit
				temp.save()
				connect_auth_db.commit()
				connect_auth_db.close()				
				return redirect("chatapp:login")
		else:
			print(form.errors)
			return HttpResponse("Form Not Valid")

def login_user(request):
	if request.method == "GET":
		return render(request, "chatbook_login.html")
	if request.method == "POST":
		params = request.POST
		email = params["email"]
		password = params["password"]
		user = authenticate(email = email.lower(), password = password)
		if user:
			user.token = tokengen()
			user.save()
			login(request, user)
			return redirect("chatapp:inbox", uuid = user.uuid)
		else:
			return HttpResponse("User Credentials Not Valid")

@login_required(login_url='/login')
def logout_user(request):
	logout(request)
	return render(request,"chatbook_login.html")

@login_required(login_url='/login')
def inbox(request, uuid):
	if request.method == "GET":
		sub_topic_list = []
		sub_dict={}
		pub_topic_list = []
		pub_dict = {}
		user = ChatBookUsers.objects.get(uuid = uuid)
		user_list = ChatBookUsers.objects.all().exclude(uuid = user.uuid)
		count = 1
		for friends in user_list:
			dummy_subtopic = "chatbook/"+friends.username+"/"+user.username
			dummy_pubtoic = "chatbook/"+user.username+"/"+friends.username
			sub_dict["id"]=str(count)
			sub_dict["usercounter"] = friends.username
			sub_dict["topic"]=dummy_subtopic
			pub_dict["id"]=str(count)
			pub_dict["topic"]=dummy_pubtoic
			count += 1
			sub_topic_list.append(sub_dict.copy())
			pub_topic_list.append(pub_dict.copy())
		ping_sub_topic = "chatbook/"+user.username+"/ping"
		return render(request, "inbox.html", {'uuid':uuid, 'user_list':user_list, "sub_topic_list":sub_topic_list, "pub_topic_list":pub_topic_list, "ping_sub_topic":ping_sub_topic})

@csrf_exempt
@login_required(login_url='/login')
def save_message_sent(request):
	if request.method == "POST":
		try:
			params = request.POST
			filter_list = params["pub_topic"].split("/")			
			sent_to = filter_list[2]
			sent_from = filter_list[1]
			payload_sent = params["payload_sent"]
			user = ChatBookUsers.objects.get(username = sent_from)
			message_obj = MessagesSent(user = user, sent_to = sent_to, payload = payload_sent, identity = "sender")
			message_obj.save()
			return JsonResponse({'status':'200'})
		except Exception as e:
			print(e)
			return JsonResponse({'status':'400'})

@csrf_exempt
@login_required(login_url='/login')
def save_message_received(request):
	if request.method == "POST":
		try:
			params = request.POST
			filter_list = params["sub_topic"].split("/")
			sent_by = filter_list[1]
			rec_from = filter_list[2]
			payload_rec = params["payload_rec"]
			user = ChatBookUsers.objects.get(username = rec_from)
			message_obj = MessagesReceived(user = user, sent_by = sent_by, payload = payload_rec, identity = "receiver")
			message_obj.save()
			return JsonResponse({'status':'200'})
		except Exception as e:
			print(e)
			return JsonResponse({'status':'400'})

@csrf_exempt
@login_required(login_url='/login')
def message_history(request):
	if request.method == "POST":
		params = request.POST
		filter_list = params["user_pub_topic"].split("/")
		sender = filter_list[1]
		receiver = filter_list[2]
		sender_user = ChatBookUsers.objects.get(username = sender)
		messages_sent_queryset = sender_user.messagessent_set.all().filter(sent_to = receiver).order_by('date_time')
		messages_received_queryset = sender_user.messagesreceived_set.all().filter(sent_by = receiver).order_by('date_time')
		#message_queryset = list(chain(messages_sent_queryset, messages_received_queryset))
		message_queryset = messages_sent_queryset.union(messages_received_queryset)
		message_queryset = message_queryset.order_by('date_time')
		message_list = []
		temp_dict = {}
		for instance in message_queryset:
			temp_dict["message"] = instance.payload
			temp_dict["identity"] = instance.identity
			temp_dict["time_stamp"] = instance.date_time
			message_list.append(temp_dict.copy())
		return JsonResponse({'status':'200', 'message_queryset':message_list})