# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-11-06 09:11
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('chatapp', '0002_remove_chatbookusers_nationality'),
    ]

    operations = [
        migrations.AddField(
            model_name='chatbookusers',
            name='sid',
            field=models.CharField(blank=True, max_length=16, null=True, unique=True, verbose_name=b'sid'),
        ),
    ]
