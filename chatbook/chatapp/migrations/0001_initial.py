# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-11-03 04:13
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ChatBookUsers',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('username', models.CharField(max_length=35, unique=True, verbose_name=b'username')),
                ('email', models.EmailField(max_length=150, unique=True, verbose_name=b'email')),
                ('first_name', models.CharField(max_length=100, verbose_name=b'first_name')),
                ('last_name', models.CharField(max_length=100, verbose_name=b'last_name')),
                ('mobile', models.CharField(max_length=15, verbose_name=b'mobile')),
                ('nationality', models.CharField(blank=True, choices=[(b'India', b'India'), (b'Afghanistan', b'Afghanistan'), (b'Australia', b'Australia'), (b'Austria', b'Austria'), (b'Bangladesh', b'Bangladesh'), (b'Belgium', b'Belgium'), (b'Bhutan', b'Bhutan'), (b'Brazil', b'Brazil'), (b'Canada', b'Canada'), (b'China', b'China'), (b'Denmark', b'Denmark'), (b'Egypt', b'Egypt'), (b'Germany', b'Germany'), (b'Indonesia', b'Indonesia'), (b'Iran', b'Iran'), (b'Kenya', b'Kenya'), (b'Myanmar', b'Myanmar'), (b'Namibia', b'Namibia'), (b'Nepal', b'Nepal'), (b'Russia', b'Russia'), (b'South Africa', b'South Africa'), (b'Sri Lanka', b'Sri Lanka'), (b'Turkey', b'Turkey'), (b'Zimbabwe', b'Zimbabwe')], max_length=100, verbose_name=b'nationality')),
                ('uuid', models.CharField(max_length=100, unique=True, verbose_name=b'urlmaker')),
                ('is_staff', models.BooleanField(default=True, verbose_name=b'staff')),
                ('is_active', models.BooleanField(default=True, verbose_name=b'active')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name_plural': 'Chat Book Users',
            },
        ),
    ]
