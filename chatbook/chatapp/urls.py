from django.conf.urls import url, include
from .views import *
from .import views
from django.conf.urls.static import static

urlpatterns = [

url(r'^signup$', views.signup_user, name='signup'),
url(r'^login$', views.login_user, name='login'),
url(r'^logout$', views.logout_user, name='logout'),
url(r'^notification_pull$', views.notification_pull, name='notification_pull'),
url(r'^set_notification_counter$', views.set_notification_counter, name='set_notification_counter'),
url(r'^reset_notification_counter$', views.reset_notification_counter, name='reset_notification_counter'),
url(r'^inbox/(?P<uuid>[-\w]+)$', views.inbox, name='inbox'),
url(r'^save_message_sent$', views.save_message_sent, name='save_message_sent'),
url(r'^save_message_received$', views.save_message_received, name='save_message_received'),
url(r'^message_history$', views.message_history, name='message_history'),
url(r'^is_token_valid$', views.is_token_valid, name='is_token_valid'),
url(r'^pull_image$', PullImage.as_view()),
]