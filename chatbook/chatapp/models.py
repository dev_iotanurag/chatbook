from django.db import models
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser

class ChatBookUsersManager(BaseUserManager):

    def create_superuser(self, email, password):
        """
        Creates and saves a superuser with the given email and password.
        """
        user = self.model(email=email)
        user.set_password(password)
        user.is_superuser = True
        user.is_staff = True
        user.save(using=self._db)
        return user


class ChatBookUsers(AbstractBaseUser):

    username = models.CharField(('username'), max_length=35, unique=True)
    token = models.CharField(('token'), max_length=500, unique=True, null = True, blank = True)
    sid = models.CharField(('sid'), max_length=16, unique=True, null = True, blank = True)
    email = models.EmailField(('email'), max_length=150, unique=True)
    first_name = models.CharField(('first_name'), max_length=100, null=False, blank=False)
    last_name = models.CharField(('last_name'), max_length=100, null=False, blank=False)
    mobile = models.CharField(('mobile'), max_length=15, null=False, blank=False)
    uuid = models.CharField(('urlmaker'), unique=True, max_length=100)
    is_staff = models.BooleanField(('staff'), default=True)
    is_active = models.BooleanField(('active'), default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = ChatBookUsersManager()
    # Field used for Authentication Purpose
    USERNAME_FIELD = 'email'
    # Field should be display as Row Object in Admin Panel
    def __str__(self):
        '''docstring for Str'''
        return self.email
    def get_full_name(self):
        '''docstring'''
        return  self.first_name+" "+self.last_name
    def get_short_name(self):
        '''docstring'''
        return  self.first_name
    def has_perm(self,perm,obj=None):
        return True#DOES USER HAS SPECIFIC PERMISSION,(SHOULD BE TRUE)
    def has_module_perms(self,app_label):
        return True#DOES USER HAS PERMISSIONS TO VIEW APP LABELS

    class Meta:
        """docstring for meta"""
        verbose_name_plural = "Chat Book Users"

class MessagesSent(models.Model):
    user = models.ForeignKey(ChatBookUsers,on_delete=models.CASCADE)
    date_time = models.DateTimeField(auto_now_add=True)
    payload = models.CharField(('payload'), max_length = 65536, null = True, blank = True)
    sent_to = models.CharField(('sent_to'), max_length = 100, null = True, blank = True)
    identity = models.CharField(('identity'), max_length = 100, default = "sender")

    def __str__(self):
        return unicode(self.user)

class MessagesReceived(models.Model):
    user = models.ForeignKey(ChatBookUsers,on_delete=models.CASCADE)
    date_time = models.DateTimeField(auto_now_add=True)
    payload = models.CharField(('payload'), max_length = 65536, null = True, blank = True)
    sent_by = models.CharField(('sent_by'), max_length = 100, null = True, blank = True)
    identity = models.CharField(('identity'), max_length = 100, default = "receiver")

    def __str__(self):
        return unicode(self.user)



